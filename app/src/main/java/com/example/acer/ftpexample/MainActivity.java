package com.example.acer.ftpexample;

import android.app.Activity;
import android.os.Environment;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;

import it.sauronsoftware.ftp4j.FTPDataTransferListener;

import static com.example.acer.ftpexample.FileUploadUsingFTP4j.createDirectory;
import static com.example.acer.ftpexample.FileUploadUsingFTP4j.deleteFile;
import static com.example.acer.ftpexample.FileUploadUsingFTP4j.uploadMulitpleFile;
import static com.example.acer.ftpexample.FileUploadUsingFTP4j.uploadSingleFile;

public class MainActivity extends Activity implements View.OnClickListener, FTPDataTransferListener {


    Button btn, b2, b3, b4, b5;
    ArrayList<File> files;
    ArrayList<String> deletefiles;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn = (Button) findViewById(R.id.b1);
        b2 = (Button) findViewById(R.id.button);
        b3 = (Button) findViewById(R.id.button2);
        b4 = (Button) findViewById(R.id.button3);
        b5 = (Button) findViewById(R.id.button4);

        btn.setOnClickListener(this);
        b2.setOnClickListener(this);
        b3.setOnClickListener(this);
        b4.setOnClickListener(this);
        b5.setOnClickListener(this);


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.b1:
                onClickCreate();

                break;
            case R.id.button:
                onClickSingleUpload();
                break;
            case R.id.button2:
                onClickMultipleUpload();
                break;
            case R.id.button3:
                onClickSingledelete();
                break;
            case R.id.button4:
                onClickMultipledelete();
                break;
        }
    }

    public void onClickCreate() {

        new Thread(new Runnable() {
            @Override
            public void run() {
                createDirectory();
            }
        }).start();

    }

    public void onClickSingleUpload() {

        /********** Pick file from sdcard *******/
        final File f = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/ftpex/Pic.jpg");

        // Upload sdcard file
        new Thread(new Runnable() {
            @Override
            public void run() {
                uploadSingleFile(f, MainActivity.this);
            }
        }).start();

    }

    public void onClickMultipleUpload() {

        /********** Pick file from sdcard *******/
        files = new ArrayList<>();

        files.add(new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/ftpex/Pic1.jpg"));
        files.add(new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/ftpex/Pic2.jpg"));


        // Upload sdcard file
        new Thread(new Runnable() {
            @Override
            public void run() {
                uploadMulitpleFile(files, MainActivity.this);
            }
        }).start();

    }

    public void onClickSingledelete() {

        /********** Pick file from sdcard *******/
        final String a ="/test/Pic.jpg";

        // Upload sdcard file
        new Thread(new Runnable() {
            @Override
            public void run() {
                FileUploadUsingFTP4j.deleteFile(a);
            }
        }).start();

    }

    public void onClickMultipledelete() {

        /********** Pick file from sdcard *******/
        final String a = "/test/Pic1.jpg";
        final String b = "/test/Pic2.jpg";
        deletefiles = new ArrayList<>();
        deletefiles.add(a);
        deletefiles.add(b);
        // Upload sdcard file
        new Thread(new Runnable() {
            @Override
            public void run() {
                FileUploadUsingFTP4j.deleteMultiple(deletefiles);
            }
        }).start();

    }



/*
    public void uploadSingleFile(File fileName) {


        FTPClient client = new FTPClient();

        try {

            client.connect(FTP_HOST, 21);
            client.login(FTP_USER, FTP_PASS);
            client.setType(FTPClient.TYPE_BINARY);
            if (client.currentDirectory().equalsIgnoreCase("/upload")) {

            } else {
                client.changeDirectory("/upload/");

            }


            //client.createDirectory("/upload/");
            File f = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/ftpex/Pic1.jpg");

         */
/*   client.upload(fileName, new MyTransferListener());
            // if multiple files need to upload
            client.append(f, new MyTransferListener());*//*

            client.deleteFile("/upload/Pic1.jpg");


        } catch (Exception e) {
            e.printStackTrace();
            try {
                client.disconnect(true);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }

    }
*/

    /*******
     * Used to file upload and show progress
     **********/

    @Override
    public void started() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                btn.setVisibility(View.GONE);
                // Transfer started
                Toast.makeText(getBaseContext(), " Upload Started ...", Toast.LENGTH_SHORT).show();
                //System.out.println(" Upload Started ...");
            }
        });
    }

    @Override
    public void transferred(final int i) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // Yet other length bytes has been transferred since the last time this
                // method was called
                Toast.makeText(getBaseContext(), " transferred ..." + i, Toast.LENGTH_SHORT).show();
                //System.out.println(" transferred ..." + length);
            }
        });
    }

    @Override
    public void completed() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                btn.setVisibility(View.VISIBLE);
                // Transfer completed

                Toast.makeText(getBaseContext(), " completed ...", Toast.LENGTH_SHORT).show();
                //System.out.println(" completed ..." );
            }
        });

    }

    @Override
    public void aborted() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                btn.setVisibility(View.VISIBLE);
                // Transfer aborted
                Toast.makeText(getBaseContext(), " transfer aborted , please try again...", Toast.LENGTH_SHORT).show();
                //System.out.println(" aborted ..." );
            }
        });
    }

    @Override
    public void failed() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                btn.setVisibility(View.VISIBLE);
                // Transfer failed
                System.out.println(" failed ...");
            }
        });

    }
}