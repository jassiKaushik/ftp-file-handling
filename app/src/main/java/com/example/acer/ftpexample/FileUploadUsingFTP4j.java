package com.example.acer.ftpexample;

import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.logging.Logger;

import it.sauronsoftware.ftp4j.FTPClient;
import it.sauronsoftware.ftp4j.FTPDataTransferListener;

/**
 * Created by acer on 25-12-2016.
 */

public class FileUploadUsingFTP4j {

    public static String TAG = "jassi";
    /*********
     * work only for Dedicated IP
     ***********/
    static final String FTP_HOST = "ftp.drivehq.com";

    /*********
     * FTP USERNAME
     ***********/
    static final String FTP_USER = "jassika";

    /*********
     * FTP PASSWORD
     ***********/
    static final String FTP_PASS = "mypapamomme";


    /***
     * This method is used to Upload file on   ftp server
     *
     * @param fileName : this is  file class instance, which indicates the fle on the local
     * @param listener : this is the listner which implement on the activity / fraggment
     */
    public static void uploadSingleFile(File fileName, FTPDataTransferListener listener) {


        FTPClient client = new FTPClient();

        try {

            client.connect(FTP_HOST, 21);
            client.login(FTP_USER, FTP_PASS);
            client.setType(FTPClient.TYPE_BINARY);
            Log.d(TAG, "connected to " + FTP_HOST + " user " + FTP_USER);
            if (client.currentDirectory().equalsIgnoreCase("/test")) {
                Log.d(TAG, " didnt need to change directory");
            } else {
                client.changeDirectory("/test/");
                Log.d(TAG, "change directory");

            }

            client.upload(fileName, listener);


        } catch (Exception e) {
            Log.d(TAG, "exception " + e.toString());

            e.printStackTrace();
            try {

                client.disconnect(true);
                Log.d(TAG, "discconted");

            } catch (Exception e2) {
                Log.d(TAG, " exception in disconnection" + e2.toString());

                e2.printStackTrace();
            }
        } finally {
            try {

                client.disconnect(true);
                Log.d(TAG, "discconted");

            } catch (Exception e2) {
                Log.d(TAG, " exception in disconnection" + e2.toString());

                e2.printStackTrace();
            }
        }

    }


    /***
     * This method is used to Upload file on   ftp server
     *
     * @param files    : this is  arrylist of files, which indicates the fles on the local
     * @param listener : this is the listner which implement on the activity / fraggment
     */

    public static void uploadMulitpleFile(ArrayList<File> files, FTPDataTransferListener listener) {


        FTPClient client = new FTPClient();

        try {

            client.connect(FTP_HOST, 21);
            client.login(FTP_USER, FTP_PASS);
            client.setType(FTPClient.TYPE_BINARY);
            Log.d(TAG, "connected to " + FTP_HOST + " user " + FTP_USER);

            if (client.currentDirectory().equalsIgnoreCase("/test")) {
                Log.d(TAG, " didnt need to change directory");

            } else {
                client.changeDirectory("/test/");
                Log.d(TAG, "change directory");

            }

            for (File file : files) {
                client.upload(file);
                Log.d(TAG, "uploaded file " + file.getPath());


            }
            Log.d(TAG, "calling listner which is impletemented on activity/fragment");

            listener.completed(); // call the  activity / fragent
            Log.d(TAG, "completed on calling");


        } catch (Exception e) {
            Log.d(TAG, "exception " + e.toString());

            e.printStackTrace();
            try {
                client.disconnect(true);
                Log.d(TAG, "disconnected");

            } catch (Exception e2) {
                Log.d(TAG, "exception in disconnection" + e.toString());

                e2.printStackTrace();
            }
        } finally {
            try {

                client.disconnect(true);
                Log.d(TAG, "discconted");

            } catch (Exception e2) {
                Log.d(TAG, " exception in disconnection" + e2.toString());

                e2.printStackTrace();
            }
        }

    }

    /***
     * This method is used to create directoy in the ftp server
     */
    public static void createDirectory() {

        FTPClient client = new FTPClient();
        try {

            client.connect(FTP_HOST, 21);
            client.login(FTP_USER, FTP_PASS);
            client.setType(FTPClient.TYPE_BINARY);
            Log.d(TAG, "connected to " + FTP_HOST + " user " + FTP_USER);

            client.createDirectory("/test/");
            Log.d(TAG, "Successfully Directory created");


        } catch (Exception e) {
            Log.d(TAG, "exception " + e.toString());

            e.printStackTrace();
            try {
                client.disconnect(true);
                Log.d(TAG, "disconnected");

            } catch (Exception e2) {
                Log.d(TAG, "exception in disconnection" + e.toString());

                e2.printStackTrace();
            }
        } finally {
            try {

                client.disconnect(true);
                Log.d(TAG, "discconted");

            } catch (Exception e2) {
                Log.d(TAG, " exception in disconnection" + e2.toString());

                e2.printStackTrace();
            }
        }

    }

    /***
     * This method allows to delete  a file fom server
     *
     * @param directoyPath : contains the path of the file on ftp server to delete it
     */

    public static void deleteFile(String directoyPath) {
        FTPClient client = new FTPClient();
        try {

            client.connect(FTP_HOST, 21);
            client.login(FTP_USER, FTP_PASS);
            client.setType(FTPClient.TYPE_BINARY);
            Log.d(TAG, "connected to " + FTP_HOST + " user " + FTP_USER);

            if (client.currentDirectory().equalsIgnoreCase("/test")) {
                Log.d(TAG, " didnt need to change directory");

            } else {
                client.changeDirectory("/test/");
                Log.d(TAG, "change directory");

            }

            client.deleteFile(directoyPath);
            Log.d(TAG, "deleted file " + directoyPath);

//  client.deleteFile("/upload/Pic1.jpg");


        } catch (Exception e) {
            Log.d(TAG, "exception " + e.toString());

            e.printStackTrace();
            try {
                client.disconnect(true);
                Log.d(TAG, "disconnected");

            } catch (Exception e2) {
                e2.printStackTrace();
                Log.d(TAG, "exception in disconnectuin" + e2.toString());

            }
        } finally {
            try {

                client.disconnect(true);
                Log.d(TAG, "discconted");

            } catch (Exception e2) {
                Log.d(TAG, " exception in disconnection" + e2.toString());

                e2.printStackTrace();
            }
        }

    }

    /***
     * This method allows to delete multiple files
     *
     * @param directoyPath : list of files on server which is about to delete
     */

    public static void deleteMultiple(ArrayList<String> directoyPath) {
        FTPClient client = new FTPClient();
        try {

            client.connect(FTP_HOST, 21);
            client.login(FTP_USER, FTP_PASS);
            client.setType(FTPClient.TYPE_BINARY);
            Log.d(TAG, "connected to " + FTP_HOST + " user " + FTP_USER);

            if (client.currentDirectory().equalsIgnoreCase("/test")) {
                Log.d(TAG, " didnt need to change directory");

            } else {
                client.changeDirectory("/test/");
                Log.d(TAG, "change directory");

            }
            for (String file : directoyPath) {
                client.deleteFile(file);
                Log.d(TAG, "deleted file " + file.toString());

            }

        } catch (Exception e) {
            Log.d(TAG, "exception " + e.toString());

            e.printStackTrace();
            try {
                client.disconnect(true);
                Log.d(TAG, "disconnected");

            } catch (Exception e2) {
                Log.d(TAG, "exception in disconnectuin" + e2.toString());

                e2.printStackTrace();
            }
        }finally {
            try {

                client.disconnect(true);
                Log.d(TAG, "discconted");

            } catch (Exception e2) {
                Log.d(TAG, " exception in disconnection" + e2.toString());

                e2.printStackTrace();
            }
        }

    }


}
